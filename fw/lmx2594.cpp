#include <libopencm3/cm3/common.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/cm3/vector.h>
#include <libopencm3/usb/usbd.h>
#include <libopencm3/efm32/cmu.h>
#include <libopencm3/efm32/gpio.h>
#include <libopencm3/efm32/usb.h>
#include <libopencm3/efm32/common/adc_common.h>
#include <libopencm3/efm32/common/usart_common.h>
#include <stdlib.h>

#define SYSTICK_FREQUENCY 10
#define AHB_FREQUENCY 24000000

#define LED_PORT GPIOA
#define LED_PIN GPIO0

#define CS0_PIN GPIO11
#define CS0_PORT GPIOB
#define CS1_PIN GPIO14
#define CS1_PORT GPIOB

#define SCK_PORT GPIOB
#define SCK_PIN GPIO13
#define MOSI_PORT GPIOB
#define MOSI_PIN GPIO7
#define MISO_PORT GPIOB
#define MISO_PIN GPIO8

/*
static uint32_t spi_32(uint32_t d) {
        // the TX buffer holds 2 bytes
        USART0_TXDATA = d >> 24;
        USART0_TXDATA = d >> 16;
        while ((USART0_STATUS & USART_STATUS_RXDATAV) == 0)
                ;
        uint32_t rv = USART0_RXDATA << 24;
        // having received 1 byte guarantees space in the TX buffer
        USART0_TXDATA = d >> 8;
        while ((USART0_STATUS & USART_STATUS_RXDATAV) == 0)
                ;
        rv |= USART0_RXDATA << 16;
        USART0_TXDATA = d;
        while ((USART0_STATUS & USART_STATUS_RXDATAV) == 0)
                ;
        rv |= USART0_RXDATA << 8;
        while ((USART0_STATUS & USART_STATUS_RXDATAV) == 0)
                ;
        rv |= USART0_RXDATA;
        return rv;
}

static uint32_t spi_pll(uint32_t d) {
        GPIO_P_DOUTCLR(CS1_PORT) = CS1_PIN;
        uint32_t rv = spi_32(d);
        GPIO_P_DOUTSET(CS1_PORT) = CS1_PIN;
        return rv;
}
*/

static uint32_t spi_ti_pll(uint32_t d) {
        GPIO_P_DOUTCLR(CS0_PORT) = CS0_PIN;
        // the TX buffer holds 2 bytes
        USART0_TXDATA = d >> 16;
        USART0_TXDATA = d >> 8;
        while ((USART0_STATUS & USART_STATUS_RXDATAV) == 0)
                ;
        uint32_t rv = USART0_RXDATA << 16;
        // having received 1 byte guarantees space in the TX buffer
        USART0_TXDATA = d;
        while ((USART0_STATUS & USART_STATUS_RXDATAV) == 0)
                ;
        rv |= USART0_RXDATA << 8;
        while ((USART0_STATUS & USART_STATUS_RXDATAV) == 0)
                ;
        rv |= USART0_RXDATA;
        GPIO_P_DOUTSET(CS0_PORT) = CS0_PIN;
        return rv;
}

static uint32_t read_adc(uint32_t input, uint32_t ref) {
        ADC0_SINGLECTRL =
            (5 << 20) | ((ref & 0x7) << 16) | ((input & 0xF) << 8);
        ADC0_CMD = ADC_CMD_SINGLESTART;
        while ((ADC0_STATUS & ADC_STATUS_SINGLEDV) == 0)
                ;
        return ADC0_SINGLEDATA;
}

static void out_cb(usbd_device *usbd_dev, uint8_t ep) {
        uint32_t buf[16] __attribute__((aligned(4)));
        uint16_t len = usbd_ep_read_packet(usbd_dev, ep, (uint8_t *)buf, 64);
        if (len < 8)
                return;
        if (buf[0] == 0) {
                uint32_t rv[4];
                rv[0] = DI_UNIQUE_0;
                rv[1] = DI_UNIQUE_1;
                rv[2] = DI_MEM_INFO_FLASH | (DI_MEM_INFO_RAM << 16);
                rv[3] = DI_PART_NUMBER | (DI_PART_FAMILY << 16) |
                        (DI_PROD_REV << 24);
                usbd_ep_write_packet(usbd_dev, 0x82, &rv[0], 16);
                return;
        }
        if (buf[0] == 3) {
                uint32_t rv = read_adc(buf[1], buf[2]);
                if (buf[1] == 8) {
                        // it's the temp sensor, convert to deg C
                        int cal_v = DI_ADC0_TEMP_0_READ_1V25 >> 4;
                        int tc = rv - cal_v;
                        // 1/6.3 deg C per ADC code, multiply by 10*65536
                        tc *= 104025;
                        tc >>= 16;
                        tc = DI_CAL_TEMP_0 * 10 - tc;
                        // tc is now 10 * temp in deg C
                        rv = tc;
                }
                usbd_ep_write_packet(usbd_dev, 0x82, &rv, 4);
                return;
        }
        if (buf[0] == 4) {
                spi_ti_pll(buf[1]);
                return;
        }
        if (buf[0] == 5) {
                uint32_t rv = spi_ti_pll(buf[1]);
                usbd_ep_write_packet(usbd_dev, 0x82, &rv, 4);
                return;
        }
        return;
}

static char serial_string[17] = "serial";
static const char manufacturer_string[] = "Harmon Instruments, LLC";
static const char product_string_lmx[] = "LMX2594 test";

static const char *usb_strings[] = {
    manufacturer_string, product_string_lmx,
    serial_string, // serial
};

extern "C" const struct usb_device_descriptor usb_dev_descr;
extern "C" const struct usb_endpoint_descriptor endp_bulk[2];
extern "C" const struct usb_interface_descriptor usb_interface_desc[1];
extern "C" const struct usb_interface usb_interfaces[1];
extern "C" const struct usb_config_descriptor usb_config_descr;

static uint8_t usbd_control_buffer[128];

static void usb_set_config(usbd_device *usbd_dev, uint16_t wValue) {
        (void)wValue;
        usbd_ep_setup(usbd_dev, 0x01, USB_ENDPOINT_ATTR_BULK, 64, out_cb);
        usbd_ep_setup(usbd_dev, 0x82, USB_ENDPOINT_ATTR_BULK, 64, NULL);
}

static usbd_device *usbd_dev;

void usb_isr(void) { usbd_poll(usbd_dev); }

void sys_tick_handler(void) { GPIO_P_DOUTTGL(LED_PORT) = LED_PIN; }

static void hex_str(uint32_t x, char *s) {
        for (int i = 0; i < 8; i++) {
                uint32_t hexit = x >> 28;
                if (hexit < 10)
                        s[i] = '0' + hexit;
                else
                        s[i] = 'A' + (hexit - 10);
                x <<= 4;
        }
}

int main(void) {
        // use the 24 MHz USB clock as our main clock
        CMU_CMD = CMU_CMD_USBCCLKSEL_USHFRCO | (5 << 0);

        // configure GPIOs
        cmu_periph_clock_enable(CMU_GPIO);
        gpio_mode_setup(LED_PORT, GPIO_MODE_PUSH_PULL, LED_PIN);
        gpio_mode_setup(CS0_PORT, GPIO_MODE_PUSH_PULL, CS0_PIN);
        gpio_mode_setup(CS1_PORT, GPIO_MODE_PUSH_PULL, CS1_PIN);
        GPIO_P_DOUTSET(CS0_PORT) = CS0_PIN;
        GPIO_P_DOUTSET(CS1_PORT) = CS1_PIN;
        gpio_mode_setup(SCK_PORT, GPIO_MODE_PUSH_PULL, SCK_PIN);
        gpio_mode_setup(MOSI_PORT, GPIO_MODE_PUSH_PULL, MOSI_PIN);
        gpio_mode_setup(MISO_PORT, GPIO_MODE_INPUT, MISO_PIN);

        // init USB
        uint32_t unique0 = DI_UNIQUE_0;
        uint32_t unique1 = DI_UNIQUE_1;
        if ((unique0 == 0x5A7B85F7) && (unique1 == 0x24A62203))
                usb_strings[1] = product_string_lmx;
        serial_string[16] = 0;
        hex_str(unique0, serial_string + 0);
        hex_str(unique1, serial_string + 8);

        usbd_dev = usbd_init(&efm32hg_usb_driver, &usb_dev_descr,
                             &usb_config_descr, usb_strings, 3,
                             usbd_control_buffer, sizeof(usbd_control_buffer));

        usbd_register_set_config_callback(usbd_dev, usb_set_config);

        nvic_set_priority(NVIC_USB_IRQ, 0x40);
        nvic_enable_irq(NVIC_USB_IRQ);

        // Configure the system tick, at lower priority than USB IRQ
        systick_set_frequency(SYSTICK_FREQUENCY, AHB_FREQUENCY);
        systick_counter_enable();
        systick_interrupt_enable();
        nvic_set_priority(NVIC_SYSTICK_IRQ, 0x10);

        // configure the USART
        cmu_periph_clock_enable(CMU_USART0);
        USART0_CTRL = USART_CTRL_MSBF | USART_CTRL_SYNC;
        USART0_FRAME = 5;                              // 8 data bits
        USART0_CLKDIV = (1 << 8);                      // 6 MHz
        USART0_ROUTE = (4 << 8) | (1 << 3) | (3 << 0); // location 4, PEN
        USART0_CMD = USART_CMD_MASTEREN | USART_CMD_TXEN | USART_CMD_RXEN;

        // configure the ADC
        cmu_periph_clock_enable(CMU_ADC0);
        ADC0_CTRL = ADC_CTRL_TIMEBASE(23) | ADC_CTRL_PRESC(1) |
                    ADC_CTRL_LPFMODE_RCFILT | ADC_CTRL_WARMUPMODE_KEEPADCWARM;

        while (1) {
        }
        return 0;
}
