#!/usr/bin/env python3
import argparse
import numpy as np
import time
import usb.core
import usb.util
from struct import pack, unpack
from datetime import datetime

class USBSpiDev:
    def __init__(self, product, serial = None):
        self.dev = None
        devs = usb.core.find(find_all = 1, idVendor=0x10c4, idProduct=0x8b00)
        print("found devices:")
        for dev in devs:
            print(dev.product, dev.serial_number)
            if dev.product == 'LMX2594 test':
                if serial is None:
                    self.dev = dev
                if dev.serial_number == serial:
                    self.dev = dev
        if self.dev is None:
            raise ValueError('Device not found')
        self.dev.set_configuration()
        self.cfg = self.dev.get_active_configuration()
        self.intf = self.cfg[(0,0)]
        print(self.dev)
        dev = self.dev
        print("Manufacturer =", usb.util.get_string(dev, dev.iManufacturer))
        print("Product = ", usb.util.get_string(dev, dev.iProduct))
        print("Serial = ", usb.util.get_string(dev, dev.iSerialNumber))

    def out_2(self, a, b):
        self.dev.write(1, pack("II", a, b))
    def out_3(self, a, b, c):
        self.dev.write(1, pack("III", a, b, c))
    def bulk_in(self, n):
        return bytes(self.dev.read(0x82, n, timeout=500))
    def bulk_in_32(self):
        (rv,) = unpack("i", self.bulk_in(4))
        return rv
    def r(self,a):
        self.out_2(5, (1<<23) | (a<<16))
        return self.bulk_in_32()
    def w(self, a, d):
        self.out_2(4, (a<<16) | (d&0xFFFF))
    def w32(self, a, d):
        self.w(a+1, d&0xFFFF)
        self.w(a, d>>16)
    def devinfo(self):
        self.out_2(0, 0)
        rv = unpack("IIII", self.bulk_in(16))
        print("uc manufactured",
              datetime.utcfromtimestamp(rv[0]).strftime('%Y-%m-%d %H:%M'))
        for i in range(4):
            print('info {} = 0x{:8X}'.format(i, rv[i]))
        return rv
    def adc(self, source, ref):
        self.out_3(3, source, ref)
        return self.bulk_in_32()
    def get_temp(self):
        return self.adc(8,0)/10.0
    def get_vtune(self):
        return 3.3*self.adc(0,2)/4096.0
    def get_vcc(self):
        return 1.25*3.0*self.adc(9,0)/4096.0

class LMX2594(USBSpiDev):
    outb_power = 31
    outa_power = 31
    mash_order = 3
    pfd_delay_sel = 3 # see N div
    pd_a = 0
    pd_b = 1
    oa_mux = 1
    pfd_freq = 100e6
    def wr37(self):
        self.w(37, (self.pfd_delay_sel<<8) | (1<<2))
    def wr44(self):
        self.w(44, (self.outa_power << 8) | (self.pd_b<<7) | (self.pd_a<<6) | (1<<5) |
               (self.mash_order<<0))
    def wr45(self):
        self.w(45, (self.oa_mux<<11) | (3<<6) | self.outb_power)
    def set_outa_power(self, p):
        self.outa_power = p
        self.wr44()
    def set_outb_power(self, p):
        self.outb_power = p
        self.wr45()
    def set_pd(self, a, b):
        self.pd_a = a
        self.pd_b = b
        self.wr44()

    def init(self):
        FCAL_HPFD_ADJ = 0
        if self.pfd_freq > 100e6:
            FCAL_HPFD_ADJ = 1
        if self.pfd_freq > 150e6:
            FCAL_HPFD_ADJ = 2
        if self.pfd_freq > 200e6:
            FCAL_HPFD_ADJ = 3
        self.w(0, (1<<1)) # reset
        self.w(0, (1<<10) | (FCAL_HPFD_ADJ) | (1<<4))
        self.wr45()
        # outputs, MASH order
        self.wr44()
        self.w32(42, 0)
        self.w32(40, 0xDEADBEEF) # MASH seed
        self.w32(38, int(self.pfd_freq)) # frac denominator
        self.wr37()
        self.w(36, 50) # PLL N low 16
        self.w(34, 0) # PLL N high
        self.w(14, (4<<4) | 0x1E00)
        self.w(12, 1 | 0x5000)
        # ref doubler
        self.w(9, (0<<12) | 0x604)
        self.w(1, 0x808)
        self.w(0, (1<<10) | (FCAL_HPFD_ADJ<<7) | (1<<4) | (1<<3) | (0<<2))
        self.w(0, (1<<10) | (FCAL_HPFD_ADJ<<7) | (1<<4) | (1<<3) | (0<<2))

    divs = [2, 4, 6, 8, 12, 16, 24, 32, 48, 64, 72, 96, 128, 192, 256, 384, 512, 768]

    def set_freq(self,f):
        chdiv = -1
        if f < 7.5e9:
            for i in range(len(self.divs)):
                chdiv = i
                if (f * self.divs[i]) > 7.5e9:
                    f *= self.divs[i]
                    break
        self.pfd_delay_sel = 3 if f < 10e9 else 4 # for frac_order = 3
        n = int(f/self.pfd_freq)
        self.w(36, n) # PLL N low 16
        frac = int(f-n*self.pfd_freq)
        print(n, frac)
        self.w32(42, frac)
        self.wr37() # pfd_delay_sel
        if chdiv >= 0:
            self.oa_mux = 0
            self.w(75, ((0 if chdiv == 2 else 1)<<11) | (chdiv<<6))
            self.w(46, 0x7FC | 0) # B channel divider enabled
        else:
            self.w(46, 0x7FC | 1) # B channel divider disabled
            self.oa_mux = 1
        self.wr45() # oa_mux
        if chdiv == 0:
            self.w(31, 0x3EC | (0<<14))
        else:
            self.w(31, 0x3EC | (1<<14))
        self.w(0, (1<<10) | (2<<7) | (1<<4) | (1<<3) | (0<<2))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--freq', type=float, help="frequency in Hz", default=10e9)
    parser.add_argument('--file', type=str, help="filename for dump data", default="dump")
    parser.add_argument('--powersweep', action='store_true', help="sweep power")
    parser.add_argument('--tunesweep', action='store_true', help="sweep tune")
    parser.add_argument('--pda', action='store_true', help="power down output A")
    parser.add_argument('--pdb', action='store_true', help="power down output B")
    parser.add_argument('--pa', type=int, help="power level A", default=31)
    parser.add_argument('--pb', type=int, help="power level B", default=31)
    parser.add_argument('--B', action='store_true', help="use B board")
    args = parser.parse_args()
    dev = LMX2594("LMX2594 test", serial='5B3FC97724F43105' if not args.B else '5B3FC98024A0E907')
    dev.devinfo()
    dev.init()
    dev.set_pd(a=1 if args.pda else 0,b=1 if args.pdb else 0)
    dev.set_outa_power(args.pa)
    dev.set_outb_power(args.pb)
    print("temp", dev.get_temp(), "°C")
    print("tune", dev.get_vtune(), "V")
    print("vcc", dev.get_vcc(), "V")

    if args.tunesweep:
        i = 0
        count = 751
        daci = np.zeros(count)
        cap = np.zeros(count)
        vt = np.zeros(count)
        f = np.linspace(7.5e9, 15e9, count)
        for i in range(len(f)):
            dev.set_freq(f[i])
            time.sleep(0.001)
            r110 = dev.r(110)
            r111 = dev.r(111)
            r112 = dev.r(112)
            daci[i] = r112
            cap[i] = r111
            vt[i] = dev.get_vtune()
        import json
        with open('dump.json', 'w') as ofile:
            json.dump({'daci':daci.tolist(),
                       'cap':cap.tolist(),
                       'vt':vt.tolist(),
                       'freq':f.tolist()},
                      ofile,
                      sort_keys=True,
                      indent=4,
                      separators=(',', ': '))
        from matplotlib import pyplot as plt
        plt.plot(f, cap, label='cap')
        plt.plot(f, daci, label='daci')
        plt.plot(f, vt*100, label='vt')
        plt.legend()
        plt.show()
    elif args.powersweep:
        import json
        import SParameter as scpp
        nrpz = scpp.NRPZ()
        freqs = np.linspace(0.5e9, 15e9, 59)[25:]
        results = []
        info = {'output': args.file}
        for i in range(len(freqs)):
            f = freqs[i]
            dev.set_freq(f)
            nrpz.set_freq(f)
            p = np.zeros(48)
            for j in range(48):
                ic = j if j < 32 else j + 16
                dev.set_outa_power(ic)
                dev.set_outb_power(ic)
                p[j] = nrpz.get_power()
            print(f, p)
            results.append(p.tolist())
        with open(args.file+'.json', 'w') as ofile:
            json.dump((info, freqs.tolist(), results),
                      ofile,
                      sort_keys=True,
                      indent=4,
                      separators=(',', ': '))
    else:
        dev.set_freq(args.freq)
        time.sleep(0.01)
        dev.w(2, 0x0100)
