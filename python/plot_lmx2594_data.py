#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
import json
import sys

with open(sys.argv[1], 'r') as ifile:
    (info, freqs, power) = json.load(ifile)

print(info, freqs)

freqs = np.array(freqs)

for i in range(len(freqs)):
    plt.plot(power[i], label=str(freqs[i]))

plt.title("LMX2594 power setting sweep {}".format(str(info)))
plt.xlabel('power setting')
plt.ylabel('power (dBm)')
plt.legend()
plt.grid(True)
plt.show()

for i in range(len(power[0])):
    p = np.zeros(len(freqs))
    for j in range(len(freqs)):
        p[j] = power[j][i]
    plt.plot(freqs/1e9, p, label=str(i))

plt.title("LMX2594 power setting sweep {}".format(str(info)))
plt.xlabel('Frequency (GHz)')
plt.ylabel('power (dBm)')
plt.legend()
plt.grid(True)
plt.show()
