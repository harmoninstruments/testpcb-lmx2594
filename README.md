# testpcb-lmx2594

A PCB, microcontroller firmware and python scripts created to evaluate the LMX2594. MIT license.

## Part Numbers

There are some Harmon Instruments house numbers.

```
8QKYD   1 IC, ESD protection for USB                    STMicroelectronics   USBLC6-2SC6            SOT23-6      0.13     0.13
RK92B   1 IC, PLL, VCO, 15 GHz                          Texas Instruments    LMX2594RHAT            QFN40_6x6   53.55    53.55
VFK7T   1 IC, microcontroller, EFM32 with USB           Silicon Labs         EFM32HG309F64G-B-QFN24 QFN24_5x5    1.42     1.42
BKTJX   1 IC, opamp                                     Microchip            MCP6L91T-E/OT          SOT23-5      0.36     0.36
DR2TQ   1 IC, regulator, 3.3 V, 500 mA, low noise       Texas Instruments    LP5912-3.3DRVR         DFN-6        0.51     0.51
YY3BC   2 capacitor, NP0, 1 nF, 50, 5%                  Murata               GRM1555C1H102JA01D     0402         0.01     0.01
QVMT6   1 capacitor, NP0, 100 nF, 50 V, 5%              Murata               GRM31C5C1H104JA01K     1206         0.19     0.19
8Q7VX  14 capacitor, X5R, 1 μF, 10 V, 10%               Murata               GRM155R61A105KE15D     0402          0.0     0.04
VY3CG   4 capacitor, X5R, 10 μF, 16 V, 10%              Murata               GRM21BR61C106KE15L     0805         0.02     0.07
HMXXW   2 capacitor, X7R, 100 nF, 16 V, 10%             Murata               GRM155R71C104KA88D     0402          0.0      0.0
GJB4W   2 capacitor, feedthrough, 6.3 V, X5R, 1 μF      Murata               NFM18PS105D0J3D        0603_3       0.03     0.07
JVWW9   1 connector, USB micro B                        Amphenol             10118192-0001LF        USB_micro_B  0.25     0.25
QF838   2 diode, LED, green                             Osram                LG L29K-G2J1-24-Z      0603          0.1      0.2
44FWG   2 ferrite bead, 470 ohm @ 100 MHz, 200 mA       Murata               BLM15GG471SN1D         0402         0.17     0.34
6JDWW   2 resistor, thin film, 20, 0.5%                 Susumu               RR0510R-200-D          0402         0.01     0.02
6F9JG   2 resistor, thin film, 4.99 k, 0.5%             Susumu               RR0510P-4991-D         0402         0.01     0.02
C4RQ2   4 resistor, thin film, 49.9, 0.5%               Susumu               RR0510R-49R9-D         0402         0.01     0.03
W8R76   1 shield frame                                  Laird                BMI-S-230-F-R          BMI-S-230F   1.18     1.18
CT6XQ   1 transformer, balun, 4.5 - 3000 MHz            Macom                MABA-007159            SM-22         0.7      0.7
15      2 15 ohm 1% 0402 resistor
c3n30   1 3.3 nF NP0 0805 capacitor
1 nF    4 1 nF NP0 0201 capacitor
coax_compression   5 - see Connectors comment below
```

### Connectors

See [breakout-pcbs](https://gitlab.com/harmoninstruments/breakout-pcbs) for options.

## Output power vs power setting

![](doc/OB_ferrite.png)

![](doc/OA_resistor.png)

## Programming

You will need some `arm-none-eabi-g++` in your path to compile.

libopencm3 is included as a submodule. `git submodule init`, `git
submodule update`, `cd fw/libopencm3; make -j$(nproc)`

`make flash` will program the board assuming it is connected with [this](https://github.com/HarmonInstruments/JTAG_SWD) SWD adapter. The openocd config files can be modified for other programmers.

## Control

The included Python script expects a 100 MHz reference clock on P320.

The frequency can be set to 10 GHz using `python3 lmx2594.py
10e9`. There are some other commands to generate plots. `tunesweep`
will export tuning voltage info as `dump.json`. `powersweep` will
generate JSON data on output power, but requires an NRP-Z55 power
sensor and additional libaries.